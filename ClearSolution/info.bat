@echo off

rem 跳转标签
rem %1: 标签
goto :%1

:start
set /p sure=确认清理(Y/N): 
if /i "%sure%"=="Y" goto :main
if /i "%sure%"=="N" exit

:section
rem %2: 节名
echo 正在清理项目 : %2
goto main

:key
rem %2: 键名
goto main

:value
rem %2: 键值
goto main

:file_before
rem %2: 文件路径
echo 清理文件 %2
goto main

:file_after
rem %2: 文件路径
goto main

:folder_before
rem %2: 文件夹路径
echo 清理文件夹 %2
goto main

:folder_after
rem %2: 文件夹路径
goto main

:end
echo. & set /p="已完成项目清理,按任意键退出" <nul
goto main

rem 虚入口
:main