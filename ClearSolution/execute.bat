@echo off
@color 0a
@title VS2010 清理工具 By GBD

echo                               VS2010 清理工具 By GBD
echo --------------------------------------------------------------------------------

echo 当前路径(dir): %cd% & echo.

call info.bat start

:clear

rem 使用变量延时
setlocal enabledelayedexpansion

rem 根目录
set "root=%cd%"

for /f "eol=# tokens=1* delims==" %%1 in (config.ini) do (
	set "token=%%1"
	
	rem 节名,键名与键值
	if "!token:~0,1!"=="[" (
		if "!token:~-1!"=="]" (
			echo. & set "path=!root!"
			rem 节名
			set "section=!token:~1,-1!"
			call info.bat section !section!
		)
	) else (
		rem 键名
		set "key=%%1"
		call info.bat key %%1
		
		rem 键值
		set "value=%%2"
		call info.bat value %%2
	)
	
	rem 节名与键名
	if not "!section!"=="" (
		if "!key!"=="root" (
			if not "!value!"=="" (
				set "path=!root!!value:%%root%%=!"
			) else (
				set "path=!root!"
			)
		) else if "!key!"=="path" (
			set "list=!value!"
		) else if "!key!"=="file" (
			set "pathlist=!list!"
			call :pathlist
		) else if "!key!"=="folder" (
			set "pathlist=!list!"
			call :pathlist
		)
	) else (
		if "!key!"=="root" (
			set "root=!value!"
		)
	)
	
	rem 清空变量
	set "key="
	set "value="
)

rem 清空变量
set "token="
set "section="
set "path="
set "root="

rem 取消变量延时
endlocal

call info.bat end
pause>nul & exit

:pathlist
if "%pathlist%"=="" (
	rem 临时路径
	set "localpath=%path%"
	
	rem 文件列表
	set "filelist=%value%"
	
	call :filelist
) else (
	for /f "tokens=1* delims=|" %%1 in ("%pathlist%") do (
		rem 临时路径
		set "localpath=%path%\%%1"
		
		rem 文件列表
		set "filelist=%value%"
		
		rem 使用变量延时
		setlocal enabledelayedexpansion
		
		call :filelist
		
		rem 取消变量延时
		endlocal
		
		set "pathlist=%%2"
	)
)
if defined pathlist (
	goto :pathlist
) else (
	rem 清空变量
	set "filelist="
	set "pathlist="
	set "localpath="
	
	goto :eof
)

:filelist
for /f "tokens=1* delims=|" %%1 in ("!filelist!") do (
	rem 临时路径
	set "localfile=!localpath!\%%1"
	
	rem 清空数据
	if "!key!"=="file" (
		call info.bat file_before !localfile!
		
		rem 命令接口
		call interface.bat !localfile!
		
		call info.bat file_after !localfile!
	) else if "!key!"=="folder" (
		call info.bat folder_before !localfile!
		
		rem 命令接口
		call interface.bat !localfile!
		
		call info.bat folder_after !localfile!
	)
	
	set "filelist=%%2"
)
if defined filelist (
	goto :filelist
) else (
	goto :eof
)
