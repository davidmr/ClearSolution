 **简介** 

VS项目清理小工具，通过INI配置文件快速清理项目，清除无用的数据库等文件与文件夹，简单实用。

 **支持平台** 

Windows

 **开源许可** 

ClearSolution 遵循 [Apache 协议](https://git.oschina.net/GBDGame/ClearSolution/blob/master/LICENSE)，使用时请标注[原作者](https://my.oschina.net/GBDGame)。

 **项目展示** 

![Author 南城繁星](http://git.oschina.net/uploads/images/2016/0912/204402_1525e6b4_333485.png "ClearSolution")